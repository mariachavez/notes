Notes
=====
My attempt at notetaking with LaTeX. The document class I am using is based off Stephano Maggiolo's class. 
I hope to have notes for all the math-oriented courses I'll be taking in the fall. 
Note: since this is my first time writing on LaTex my notes will probably have errors. Also, the problem solutions were worked out by me and are not yet graded. If you spot an error please point it out. 

## Document Structure
```tex
  \documentclass[english,course]{Notes}
  
  \title{Title}
  \subject{Subject}
  \author{Maria Chavez}
  \email{materechm@gmail.com}
  \speaker{Name}
  \date{dd}{mm}{year}
  \dateend{dd}{mm}{year}
  \place{Location}
  
  
  \begin{document}
  
  \end{document}
```

## Currently on this repository 
* Math 18.01 notes (Single variable calc course on MIT OCW) 
* Problems and solutions for some calc problems 
* Visual complex analysis (in progress) 
* CS106A (programming methodologies course at Stanford, in progress) 

## Upcoming 
* CS106B (programming abstractions course at Stanford)
* CS103 (mathematical foundations of computing course at Stanford)
* MATH120 (groups and rings course at Stanford)
* MATH215A (complex analysis, geometry and topology course at Stanford)
* MATH51 (linear algebra and differential calculus of several variables couse at Stanford)
* Potentially some algebra/number theory from math olympiad training 
